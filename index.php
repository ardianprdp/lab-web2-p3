<?php

# include file koneksi
include_once 'koneksi.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tugas Kelas 207 | 30721087 - Ardi Ananta Pradipta</title>
  <link rel="icon" href="logo.ico" type="image/ico" sizes="16x16">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
      <a class="navbar-brand" href="#">Tugas Pertemuan 3</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Features</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Pricing</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">disabled</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container p-4">

    <div class="row mb-3">
      <div class="col">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambah">
          Tambah Data
        </button>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col">
        <table class="table">
          <thead class="table-dark">
            <tr>
              <th class="text-center" scope="col" width="5%">No</th>
              <th class="text-center" scope="col" width="10%">NPM</th>
              <th class="text-center" scope="col" width="">Nama</th>
              <th class="text-center" scope="col" width="">Alamat</th>
              <th class="text-center" scope="col" width="10%">No HP</th>
              <th class="text-center" scope="col" width="15%">Aksi</th>
            </tr>
          </thead>
          <tbody>

            <?php

            $nomor = 1;

            # get data mahasiswa
            $data = mysqli_query($koneksi, "SELECT * FROM mahasiswa");

            # jika tidak ada data
            if(mysqli_num_rows($data) > 0) {

              while ($mhs = mysqli_fetch_assoc($data)) { ?>

                <tr>
                  <th class="text-center" scope="row"><?= $nomor++ ?></th>
                  <td class="text-center"><?= $mhs['npm'] ?></td>
                  <td class=""><?= $mhs['nama'] ?></td>
                  <td class=""><?= $mhs['alamat'] ?></td>
                  <td class="text-center"><?= $mhs['no_hp'] ?></td>
                  <td class="text-center">

                    <button class="btn btn-sm btn-icon btn-warning btnEdit" data-toggle="modal" data-target="#modalEdit" 
                        data-id="<?= $mhs['id'] ?>" 
                        data-nama="<?= $mhs['nama'] ?>" 
                        data-npm="<?= $mhs['npm'] ?>" 
                        data-alamat="<?= $mhs['alamat'] ?>" 
                        data-no_hp="<?= $mhs['no_hp'] ?>">
                      Edit
                    </button>
                    
                    <a href="hapus.php?id=<?= $mhs['id'] ?>" class="btn btn-sm btn-icon btn-danger">
                      Hapus
                    </a>

                  </td>
                </tr>

              <?php } } else { ?>

              <td class="text-center" colspan="6">Tidak terdapat data</td>

            <?php } ?>

          </tbody>
        </table>
      </div>
    </div>

    <!-- Modal Edit Mahasiswa -->
    <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalTambahLabel">Tambah Data Mahasiswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="post" action="tambah.php">
            <div class="modal-body m-3">
              <div class="row mb-3">
                <div class="col">
                  <label for="nama" class="col-form-label">Nama:</label>
                  <input type="text" class="form-control" name="nama">
                </div>
                <div class="col">
                  <label for="npm" class="col-form-label">NPM:</label>
                  <input type="text" class="form-control" name="npm">
                </div>
              </div>
              <div class="mb-3">
                <label for="alamat" class="col-form-label">Alamat:</label>
                <textarea class="form-control" name="alamat" rows="4"></textarea>
              </div>
              <div class="mb-3">
                <label for="no_hp" class="col-form-label">No HP:</label>
                <input type="text" class="form-control col-6" name="no_hp">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal Tambah Mahasiswa -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalEditLabel">Edit Data Mahasiswa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="post" action="update.php">
            <div class="modal-body m-3">
              <input type="hidden" class="form-control" name="id">
              <div class="row mb-3">
                <div class="col">
                  <label for="nama" class="col-form-label">Nama:</label>
                  <input type="text" class="form-control" name="nama">
                </div>
                <div class="col">
                  <label for="npm" class="col-form-label">NPM:</label>
                  <input type="text" class="form-control" name="npm">
                </div>
              </div>
              <div class="mb-3">
                <label for="alamat" class="col-form-label">Alamat:</label>
                <textarea class="form-control" name="alamat" rows="4"></textarea>
              </div>
              <div class="mb-3">
                <label for="no_hp" class="col-form-label">No HP:</label>
                <input type="text" class="form-control col-6" name="no_hp">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <script>
    $(".btnEdit").click(function () {

        $("#modalEdit input[name=id]").val($(this).data("id")).change();
        $("#modalEdit input[name=nama]").val($(this).data("nama")).change();
        $("#modalEdit input[name=npm]").val($(this).data("npm")).change();
        $("#modalEdit textarea[name=alamat]").val($(this).data("alamat")).change();
        $("#modalEdit input[name=no_hp]").val($(this).data("no_hp")).change();
    });
  </script>
        


</body>

</html>